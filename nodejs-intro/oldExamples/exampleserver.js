app.get(
  '/test(.html)?',
  (req, res, next) => {
    console.log('attempting to test page');
    next();
  },
  (req, res) => {
    res.send('test world');
  }
);

//chaining routes
const one = (req, res, next) => {
  console.log('one');
  next();
};

const two = (req, res, next) => {
  console.log('two');
  next();
};

const three = (req, res) => {
  console.log('three');
  res.send('finished');
};

app.get('/chain(.html)?', [one, two, three]);
