const text = document.getElementById( 'notify-text' ); 
const notify = document.getElementById( 'notify-button' ); 
const reset = document.getElementById( 'notify-reset' ); 
const counter = document.getElementById( 'notify-count' );

//gets notification stored
chrome.storage.local.get( ['notifyCount'], data => {
	let value = data.notifyCount || 0;
	counter.innerHTML = value;
} );

// get value from inner notifications
chrome.storage.onChanged.addListener( ( changes, namespace ) => {
	if ( changes.notifyCount ) {
		let value = changes.notifyCount.newValue || 0;
		counter.innerHTML = value;
	}
});


//reset value
reset.addEventListener( 'click', () => {
	chrome.storage.local.clear();
	text.value = '';
} );


notify.addEventListener( 'click', () => {
	chrome.runtime.sendMessage( '', {
		type: 'notification',
		message: text.value
	});
} );
