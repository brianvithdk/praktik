//used to get notification
chrome.runtime.onMessage.addListener((data) => {
  if (data.type === 'notification') {
    notify(data.message);
  }
});

//used for the right clicks
chrome.runtime.onInstalled.addListener(() => {
  chrome.contextMenus.create({
    id: 'notify',
    title: 'Notify!: %s',
    contexts: ['selection'],
  });
});

//used for the right clicks click event
chrome.contextMenus.onClicked.addListener((info, tab) => {
  if ('notify' === info.menuItemId) {
    notify(info.selectionText);
  }
});

const notify = (message) => {
  //used for the notifcation count only
  chrome.storage.local.get(['notifyCount'], (data) => {
    let value = data.notifyCount || 0;
    chrome.storage.local.set({ notifyCount: Number(value) + 1 });
  });

  //used to create a notification
  return chrome.notifications.create('', {
    type: 'basic',
    title: 'Notify!',
    message: message || 'Notify!',
    iconUrl: './assets/icons/128.png',
  });
};
